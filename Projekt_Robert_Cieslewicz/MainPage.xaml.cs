﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace Projekt_Robert_Cieslewicz
{
    public sealed partial class MainPage : Page
    {
        string napis1 = "", napis2 = "", napis3 = "", napis4 = "", szyfr = "ROT13", haslo = "qwerty", przesuniecie = "3"; //Wiadomosci oraz domyslne hasla
        int value1 = 40, value2 = 50; //Domyslne wartosci ProgressBara

        private int mod(int x, int m) //funkcja modulo- ta wbudowana zle dziala!!!
        {
            return ((x % m + m) % m);
        }

        private void szyfruj()
        {
            napis2 = ""; //Zaszyfrowana wiadomosc
            for(int i=0;i<napis1.Length;i++)
            {
                int wartosc = Convert.ToInt16(napis1[i]); //Wartosc nowego znaku
                if (szyfr == "ROT13")
                {
                    if (wartosc >= 65 && wartosc <= 90) //Wprowadzono litere
                    {
                        wartosc = (mod((wartosc - 52), 26)) + 65; //Szyfrowanie ROT13
                    }
                    napis2 += (Convert.ToChar(wartosc)).ToString(); //Zapisanie zaszyfrowanej litery
                }
                else if (szyfr == "PRZESUNIECIE")
                {
                    if (wartosc >= 65 && wartosc <= 90) //Wprowadzono litere
                    {
                        wartosc = (mod(((wartosc - 65) + Convert.ToInt16(przesuniecie)), 26)) + 65; //Szyfrowanie
                    }
                    napis2 += (Convert.ToChar(wartosc)).ToString(); //Zapisanie zaszyfrowanej litery
                }
                else if(szyfr == "HASLO")
                {
                    int temp = ((Convert.ToInt16(haslo[i % haslo.Length])) - 65); //Wartosc dobranej litery
                    if(wartosc>=65 && wartosc<= 90) //Wprowadzono litere
                    {
                        wartosc = (mod(((wartosc - 65) + temp), 26)) + 65; //Szyfrowanie haslem
                    }
                    napis2 += (Convert.ToChar(wartosc)).ToString(); //Zapisanie zaszyfrowanej litery
                }
            }
        }

        private void deszyfruj()
        {
            napis4 = ""; //Odszyfrowana wiadomosc
            for (int i = 0; i < napis3.Length; i++)
            {
                int wartosc = Convert.ToInt16(napis3[i]); //Wartosc nowego znaku
                if (szyfr == "ROT13")
                {
                    if (wartosc >= 65 && wartosc <= 90) //Wprowadzono litere
                    {
                        wartosc = (mod((wartosc - 78), 26)) + 65; //Deszyfrowanie ROT13
                    }
                    napis4 += (Convert.ToChar(wartosc)).ToString(); //Zapisanie odszyfrowanej litery
                }
                else if (szyfr == "PRZESUNIECIE")
                {
                    if (wartosc >= 65 && wartosc <= 90) //Wprowadzono litere
                    {
                        wartosc = (mod(((wartosc - 65) - Convert.ToInt16(przesuniecie)), 26)) + 65; //Deszyfrowanie
                    }
                    napis4 += (Convert.ToChar(wartosc)).ToString(); //Zapisanie odszyfrowanej litery
                }
                else if (szyfr == "HASLO")
                {
                    int temp = ((Convert.ToInt16(haslo[i % haslo.Length])) - 65); //Wartosc dobranej litery
                    if (wartosc >= 65 && wartosc <= 90) //Wprowadzono litere
                    {
                        wartosc = (mod(((wartosc - 65) - temp), 26)) + 65; //Odszyfrowanie haslem
                    }
                    napis4 += (Convert.ToChar(wartosc)).ToString(); //Zapisanie odszyfrowanej litery
                }
            }
        }

        public MainPage()
        {
            this.InitializeComponent();
        }

        //WPROWADZANIE TEKSTU:
        private void textBox1_TextChanged(object sender, TextChangedEventArgs e) //Wpisywanie tekstu
        {
            napis1 = textBox1.Text.ToUpper(); //Wprowadzanie wiadomosci
            szyfruj(); //Szyfrowanie
            textBox2.Text = napis2; //Wyswietlenie wynikowej wiadomosci
        }

        private void textBox3_TextChanged(object sender, TextChangedEventArgs e) //Wpisywanie tekstu
        {
            napis3 = textBox3.Text.ToUpper(); //Wprowadzanie wiadomosci
            deszyfruj(); //Deszyfrowanie
            textBox4.Text = napis4; //Wyswietlenie wynikowej wiadomosci
        }

        private void passwordBox1_PasswordChanged(object sender, RoutedEventArgs e) //Wprowadzanie przesuniecia
        {
            przesuniecie = passwordBox1.Password; //Odczytanie przesuniecia
            if (przesuniecie != "3" && przesuniecie != "0") //Haslo domyslne
            {
                value1 = 50; //Aktualizacja ProgressBara
            }
            else
            {
                value1 = 40; //Aktualizacja ProgressBara
            }
            ProgressBar.Value = value1; //Aktualizacja ProgressBara
        }

        private void passwordBox2_PasswordChanged(object sender, RoutedEventArgs e) //Wprowadzanie hasla
        {
            haslo = passwordBox2.Password.ToUpper(); //Odczytanie hasla
            if (haslo == "QWERTY") //Haslo domyslne
            {
                value2 = 50; //Aktualizacja ProgressBara
            }
            else
            {
                if (haslo.Length < 4)
                {
                    value2 = 55; //Aktualizacja ProgressBara
                }
                else if (haslo.Length >= 4 && haslo.Length <= 6)
                {
                    value2 = 60 + 5 * (haslo.Length - 4); //Aktualizacja ProgressBara
                }
                else
                {
                    value2 = 70 + 3 * (haslo.Length - 6); //Aktualizacja ProgressBara
                }
            }
            ProgressBar.Value = value2; //Aktualizacja ProgressBara
        }

        //PRZYCISKI:
        private void radioButton1_Checked(object sender, RoutedEventArgs e)
        {
            szyfr = "ROT13";
            passwordBox1.IsEnabled = false; //Widocznosc PasswordBoxa
            passwordBox2.IsEnabled = false; //Widocznosc PasswordBoxa
            ProgressBar.Value = 30; //Aktualizacja ProgressBara
        }

        private void radioButton2_Checked(object sender, RoutedEventArgs e)
        {
            szyfr = "PRZESUNIECIE";
            passwordBox1.IsEnabled = true; //Widocznosc PasswordBoxa
            passwordBox2.IsEnabled = false; //Widocznosc PasswordBoxa
            ProgressBar.Value = value1; //Aktualizacja ProgressBara
        }

        private void radioButton3_Checked(object sender, RoutedEventArgs e)
        {
            szyfr = "HASLO";
            passwordBox1.IsEnabled = false; //Widocznosc PasswordBoxa
            passwordBox2.IsEnabled = true; //Widocznosc PasswordBoxa
            ProgressBar.Value = value2; //Aktualizacja ProgressBara
        }  
    }
}
